#!/bin/bash

test -z "$firmware" && source ./firmware true
local_read smali
test -z "$1" -o -z "$smali" && \
menu smali "$(find "$tools" -type f -name "smali*.jar" -printf "%f\n" | sort -u)" && \
local_write smali
smali="$java -jar $tools/$smali"
